import java.util.*;

public class Lab6App {

    public static void main(String[] args) {
        TreeMap<NrTelefoniczny, Wpis> tmap = PhoneBookInitializer.init();

        Set set = tmap.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            System.out.print(((Wpis)entry.getValue()).opis()+"\n");
        }
    }
}

class PhoneBookInitializer{

    static TreeMap<NrTelefoniczny, Wpis> init(){
        TreeMap<NrTelefoniczny, Wpis> tmap = new TreeMap<NrTelefoniczny, Wpis>();

        ArrayList<Wpis> wpisy = new ArrayList<Wpis>();
        wpisy.add(new Osoba("Jan", "Kowalski", "Biała 1", "48", "2761010" ));
        wpisy.add(new Osoba("Adam", "Nowak", "Czerwona 4", "48", "1232020" ));
        wpisy.add(new Osoba("Jarosław", "Psikuta", "Warszawska", "48", "2780303" ));
        wpisy.add(new Osoba("Maciej", "Poborca", "Kolumna", "48", "997" ));
        wpisy.add(new Osoba("Adrian", "Szymański", "Szadek", "48", "5658274" ));

        wpisy.add(new Firma("Torebki Maciusia", "Bałucki", "48", "1231212"));
        wpisy.add(new Firma("Vape Nation", "Szadek", "48", "5658282"));
        wpisy.add(new Firma("Solar Team", "Politechnika Łódzka", "48", "1112323"));
        wpisy.add(new Firma("Ghor", "Nikt nie wie", "48", "2222222"));
        wpisy.add(new Firma("Zahir", "Zawsze za rogiem", "48", "1111111"));

        for (Wpis wpis : wpisy) {
            tmap.put(wpis.telefon(), wpis);
        }

        return tmap;
    }
}

class NrTelefoniczny implements Comparable<NrTelefoniczny>{

    private String nrKierunkowy;
    private String nrTelefonu;

    public NrTelefoniczny(String nrKierunkowy, String nrTelefonu){
        this.nrKierunkowy = nrKierunkowy;
        this.nrTelefonu = nrTelefonu;
    }

    @Override
    public int compareTo(NrTelefoniczny nrTelefoniczny) {
        return (this.nrKierunkowy+this.nrTelefonu).compareTo(nrTelefoniczny.getNrKierunkowy()+nrTelefoniczny.getNrTelefonu());
    }

    public String getNrKierunkowy() {
        return nrKierunkowy;
    }

    public void setNrKierunkowy(String nrKierunkowy) {
        this.nrKierunkowy = nrKierunkowy;
    }

    public String getNrTelefonu() {
        return nrTelefonu;
    }

    public void setNrTelefonu(String nrTelefonu) {
        this.nrTelefonu = nrTelefonu;
    }
}

abstract class Wpis {

    public abstract String opis();

    public abstract NrTelefoniczny telefon();

}

class Osoba extends Wpis {

    private String imie;
    private String nazwisko;
    private String adres;
    private NrTelefoniczny telefon;

    public Osoba(String imie, String nazwisko, String adres, NrTelefoniczny telefon){
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.adres = adres;
        this.telefon = telefon;
    }

    public Osoba(String imie, String nazwisko, String adres, String nrKierunkowy, String nrTelefonu){
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.adres = adres;
        this.telefon = new NrTelefoniczny(nrKierunkowy, nrTelefonu);
    }

    public String opis() {
        StringBuilder sb = new StringBuilder();
        sb.append("Imie i nazwisko: ").append(this.imie).append(" ").append(this.nazwisko).append("\n");
        sb.append("Adres: ").append(this.adres).append("\n");
        sb.append("Telefon: +").append(this.telefon.getNrKierunkowy()).append(" ").append(this.telefon.getNrTelefonu()).append("\n");
        return sb.toString();
    }

    public NrTelefoniczny telefon(){
        return this.telefon;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public NrTelefoniczny getTelefon() {
        return telefon;
    }

    public void setTelefon(NrTelefoniczny telefon) {
        this.telefon = telefon;
    }
}

class Firma extends Wpis {

    private String nazwa;
    private String adres;
    private NrTelefoniczny telefon;

    public Firma(String nazwa, String adres, NrTelefoniczny telefon){
        this.nazwa = nazwa;
        this.adres = adres;
        this.telefon = telefon;
    }

    public Firma(String nazwa, String adres, String nrKierunkowy, String nrTelefonu){
        this.nazwa = nazwa;
        this.adres = adres;
        this.telefon = new NrTelefoniczny(nrKierunkowy, nrTelefonu);
    }

    public String opis() {
        StringBuilder sb = new StringBuilder();
        sb.append("Nazwa firmy: ").append(this.nazwa).append("\n");
        sb.append("Adres: ").append(this.adres).append("\n");
        sb.append("Telefon: +").append(this.telefon.getNrKierunkowy()).append(" ").append(this.telefon.getNrTelefonu()).append("\n");
        return sb.toString();
    }

    public NrTelefoniczny telefon(){
        return this.telefon;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public NrTelefoniczny getTelefon() {
        return telefon;
    }

    public void setTelefon(NrTelefoniczny telefon) {
        this.telefon = telefon;
    }
}