package com.company;

import java.util.*;
import java.io.*;

class WektoryRoznejDlugosciException extends Exception {
    public WektoryRoznejDlugosciException(String message){
        super(message);
    }
}

public class VectorHandler{

    public static Vector<Double> readVector(){
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(input);
        Vector<Double> vector = new Vector<Double>();

        try {
            Scanner scan = new Scanner(reader.readLine());

            while(scan.hasNext()){
                if(scan.hasNextDouble()){
                    vector.add(scan.nextDouble());
                } else {
                    scan.next();
                }
            }
            scan.close();

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        return vector;
    }

    public static Vector<Double> addVectors(Vector<Double> vecA, Vector<Double> vecB) throws WektoryRoznejDlugosciException {
        Vector<Double> C = new Vector<Double>();
        if(vecA.size() == vecB.size()){
            for(int i=0; i<vecA.size(); i++){
                C.add(vecA.elementAt(i) + vecB.elementAt(i));
            }
        } else {
            throw new WektoryRoznejDlugosciException("Vectors are of different sizes!");
        }
        return C;
    }

    public static boolean saveResult(String path, Vector<Double> vec){
        try {
            FileOutputStream output = new FileOutputStream(path);
            String vecString = vec.toString();
            for(int i=0; i<vecString.length(); i++) {
                output.write(vecString.toCharArray()[i]);
            }
            output.close();
        } catch (IOException ex){
            System.out.println("IOException: "+ex.getMessage());
            return false;
        }
        System.out.println("Vector successfuly saved at \""+path+"\"!");
        return true;
    }
}