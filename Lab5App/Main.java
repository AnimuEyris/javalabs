import java.util.*;
import java.io.*;

public class Main {

    public static void main(String[] args) {
        Vector<Double> A = null, B = null, C = null;
	VectorHandler vecHandler = new VectorHandler();
	FileSaver fSaver = new FileSaver();
        
        do {
            System.out.println("Enter two vectors:");
            
            try { 
	    
		A = vecHandler.readVector();
		B = vecHandler.readVector();
		
		C = vecHandler.addVectors(A, B);
		
	    } catch ( WektoryRoznejDlugosciException ex ) {
		System.out.println( ex.getMessage() + "\nVecA size: " + ex.getSizeA() + "\nVecB size: " + ex.getSizeB());
	    } catch ( IllegalArgumentException ex ) {
	        System.out.println( ex.getMessage() );
            }
            
        } while (C == null);

        System.out.println("The vector sum is equal to:\n"+C.toString());
        fSaver.saveToFile("/tmp/vectorSum", C);
    }
}

class WektoryRoznejDlugosciException extends Exception {
    private int sizeA, sizeB;

    public WektoryRoznejDlugosciException( String message, int sizeA, int sizeB ){
        super(message);
        this.sizeA = sizeA;
        this.sizeB = sizeB;
    }
    
    public int getSizeA(){
	return this.sizeA;
    }
    
    public int getSizeB(){
	return this.sizeB;
    }
}

class VectorHandler {

    VectorHandler(){}

    public Vector<Double> readVector() throws IllegalArgumentException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(input);
        Vector<Double> vector = new Vector<Double>();

        try {
            Scanner scan = new Scanner(reader.readLine());

            while(scan.hasNext()){
                if(scan.hasNextDouble()){
                    vector.add(scan.nextDouble());
                } else {
                    throw new IllegalArgumentException("All inputs must be numeric!");
                }
            }
            scan.close();

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        return vector;
    }

    public Vector<Double> addVectors(Vector<Double> vecA, Vector<Double> vecB) throws WektoryRoznejDlugosciException {
        Vector<Double> C = new Vector<Double>();
        if(vecA.size() == vecB.size()){
            for(int loopIterator=0; loopIterator<vecA.size(); loopIterator++){
                C.add(vecA.elementAt(loopIterator) + vecB.elementAt(loopIterator));
            }
        } else {
            throw new WektoryRoznejDlugosciException("Vectors are of different sizes!", vecA.size(), vecB.size());
        }
        return C;
    }
}

class FileSaver {
    
    FileSaver(){}
    
    public boolean saveToFile(String path, Vector<Double> vec){
        try {
            FileOutputStream output = new FileOutputStream(path);
            String vecString = vec.toString();
            for(int loopIterator=0; loopIterator<vecString.length(); loopIterator++) {
                output.write(vecString.toCharArray()[loopIterator]);
            }
            output.close();
        } catch (IOException ex){
            System.out.println("IOException: "+ex.getMessage());
            return false;
        }
        System.out.println("Vector successfuly saved at \""+path+"\"!");
        return true;
    }
    
}